// tests for a file x.go has the name x_test.go
// and they are usually in the same package
package main

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	// this is our own package in the same module
	// check the module URL in go.mod
	// "module gitlab.com/nagypalg/gowiki"
	"gitlab.com/nagypalg/gowiki/pkg/testsup"
)

func TestPort(t *testing.T) {
	assert := assert.New(t)
	oldPort := os.Getenv("PORT")
	t.Logf("Saving old port: %q", oldPort)
	// defer is Go's finally
	// it will run when we exit from the current function, for whatever reason
	// note also the unnamed function, Go's "lambda"
	defer func() {
		if len(oldPort) > 0 {
			os.Setenv("PORT", oldPort)
		}
	}()

	// Go also has maps
	fixture := map[string]string{
		"7070": ":7070",
		"xxx":  ":7654",
		"":     ":7654",
	}

	// it is easy to iterate through a map
	// k = key, v = value
	for k, v := range fixture {
		os.Setenv("PORT", k)
		actual := port()
		assert.Equal(v, actual)
	}

}

func TestDir(t *testing.T) {
	assert := assert.New(t)
	oldArgs := os.Args
	t.Logf("Saving old args: %q", oldArgs)
	// note how we avoid side effects on os.Args (that may be used by other tests)
	defer func() { os.Args = oldArgs }()

	// a slice of anonymous structs
	// a slice is like an array list in Java
	// a struct is like a C/C++ struct, a container with fields
	// it is easy to initialize them with slice literals
	// (there are also arrays, but rarely used)
	// [2]string{"one","two"} would be an array literal, note the "2" at the beginning
	tests := []struct {
		// note the nice formatting!
		args   []string // slice of strings
		expDir string   // expected directory
		expErr bool     // there are also booleans
	}{
		// structs can be initialized by named fields
		// in this case fields that were not given a value will get their zero value
		{args: []string{"default"}, expDir: defaultDir},
		{args: []string{"current", "."}, expDir: "."},
		{args: []string{"currentWithMore", ".", "xxx"}, expDir: "."},
		{args: []string{"existing", "testdata"}, expDir: "testdata"},
		{args: []string{"existingWithMore", "testdata", "xxx"}, expDir: "testdata"},
		// or by providing valuees for all fields in their declaration order
		{[]string{"missing", "missing"}, "missing", true},
	}

	// it is easy to iterate through a slice
	// the first value would be the index that we now ignore
	for _, tst := range tests {
		// this runs a subtest (which could also run a subtest etc.)
		t.Run(tst.args[0], func(t *testing.T) {
			os.Args = tst.args
			d, err := dir()
			assert.Equal(tst.expDir, d)
			assert.Equal(tst.expErr, err != nil)
		})
	}
}

func TestHTTPGet_existing(t *testing.T) {
	assert := assert.New(t)
	r := testsup.NewGetRequest(t, "/about.html")
	// built-in HTTP response recorder!
	w := httptest.NewRecorder()
	// built-in test HTTP server!
	setupHandlers("testdata").ServeHTTP(w, r)

	resp := w.Result()

	assert.Equal(http.StatusOK, resp.StatusCode)
	body := testsup.LoadRespBody(t, resp)
	// check that the result is the same as a pre-saved file
	expBody := testsup.LoadTestFile(t, "about.html")
	assert.Equal(expBody, body)

}

func TestHTTPGet_root(t *testing.T) {
	assert := assert.New(t)
	r := testsup.NewGetRequest(t, "/")
	w := httptest.NewRecorder()
	setupHandlers("testdata").ServeHTTP(w, r)

	resp := w.Result()

	assert.Equal(http.StatusOK, resp.StatusCode)
	body := testsup.LoadRespBody(t, resp)
	expBody := testsup.LoadTestFile(t, "index.html")
	assert.Equal(expBody, body)

}

func TestHTTPGet_non_existing(t *testing.T) {
	assert := assert.New(t)
	r := testsup.NewGetRequest(t, "/foo.bar")
	w := httptest.NewRecorder()
	setupHandlers("testdata").ServeHTTP(w, r)

	resp := w.Result()

	assert.Equal(http.StatusNotFound, resp.StatusCode)
	body := testsup.LoadRespBody(t, resp)
	assert.Equal("404 page not found\n", body)

}
