/*Package testsup contains some simple test support functions.
Note that this is NOT in package main, so it is a library*/
package testsup

import (
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"testing"
)

// Go has very simple visibility rules: if a name start with capital letter, it is exported (public)
// Otherwise in is visible only in the same package
// If a name is exported, you get a compile "warning" (from go lint)
// if it is undocumented, or the documentation does not start with the name

// NewGetRequest returns a new HTTP GET request against the specified path
func NewGetRequest(t *testing.T, path string) *http.Request {
	// built-in HTTP client!
	r, err := http.NewRequest("GET", path, nil)
	if err != nil {
		// Fatal will fail the test
		t.Fatal(err)
	}
	return r
}

// LoadRespBody reads the whole body of the HTTP response
func LoadRespBody(t *testing.T, resp *http.Response) string {
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatalf("Could not read response body: %s", err)
	}
	return string(b)
}

// LoadTestFile loads a file from the testdata directory
// Go has variadic parameters
func LoadTestFile(t *testing.T, pathelems ...string) string {
	// this is a "slice", which is like a Java ArrayList
	pe := []string{"testdata"}
	// the built-in append expects strings, but we can use a slice (or array) with ...
	pe = append(pe, pathelems...)
	// same here
	path := filepath.Join(pe...)
	body, err := ioutil.ReadFile(path)
	if err != nil {
		// fatal will fail the test
		t.Fatalf("failed reading test file: %s", err)
	}
	// body is []byte, not string
	// In Go there are no implicit type casts
	return string(body)
}

// CreateTempDir creates a temporary subdirectory in the OS' default temp directory
// It returns the path to the new temp directory as well as a function to clean up
func CreateTempDir(t *testing.T, dirname string) (path string, cleanup func()) {
	tmpdir, err := ioutil.TempDir("", "page_test")
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Created temp dir %q", tmpdir)
	// functions are first class citizens (we can return them and use them as parameters)
	return tmpdir, func() {
		// they are also closures (can reference variables in the current context)
		t.Logf("Deleting temp dir %q", tmpdir)
		err := os.RemoveAll(tmpdir)
		if err != nil {
			t.Fatal(err)
		}
	}
}
