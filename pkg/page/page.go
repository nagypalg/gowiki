/*
Package page provides support for loading, saving and rendering wiki pages.
*/
package page

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/golang-commonmark/markdown"
)

var pageLink = regexp.MustCompile("\\[[a-zA-Z0-9]+\\]")

// Page represents a Wiki page
type Page struct {
	Title string
	Body  []byte
}

// New creates a new page with the given title and body
func New(title string, body string) *Page {
	// Note the & operator that creates a pointer to the Page struct
	return &Page{title, []byte(body)}
}

// NewNoBody creates a page with empty body
// Note: there is no polymorphism in Go!
func NewNoBody(title string) *Page {
	return &Page{Title: title}
}

// DataDir is where the wiki pages are stored
var DataDir = "data"

// also library packages can have an init method
func init() {
	log.Println("Loading page package")
	if _, err := os.Stat(DataDir); os.IsNotExist(err) {
		log.Printf("Data dir %q does not exist, creating it", DataDir)
		os.Mkdir(DataDir, 0700)
	}
}

func makePagePath(title string) string {
	// this way this will work both on Windows and Linux/Max OS
	return filepath.Join(DataDir, title+".txt")
}

// Save saves the page to the file system
// this is a method, note the receiver argument before the method's name
func (p *Page) Save() error {
	return ioutil.WriteFile(makePagePath(p.Title), p.Body, 0600)
}

// RenderBody renders the page as HTML
func (p *Page) RenderBody() string {
	c := make(chan string)
	go renderPageBody(p, c)
	return <-c
}

func renderPageBody(p *Page, c chan string) {
	bodyWithLinks := pageLink.ReplaceAllFunc(p.Body, renderPageLink)
	md := markdown.New(markdown.XHTMLOutput(true))
	c <- md.RenderToString(bodyWithLinks)
}

func renderPageLink(pageTitle []byte) []byte {
	// relatively easy sring manipulation
	page := strings.Trim(string(pageTitle), "[]")
	// no implicit type conversions!
	return []byte(fmt.Sprintf("[%s](/view/%s)", page, page))
}

// FileReader is used to load wiki page files
// Go also has interfaces
type FileReader interface {
	ReadFile(path string) ([]byte, error)
}

type ioReader struct {
}

// A type implicitly implements an interface if it implements all its methods
// therefore *ioReader will implement fileReader
// note: ioReader will NOT implement fileReader, only *ioReader!
// Also note the named return values
func (r *ioReader) ReadFile(path string) (content []byte, err error) {
	content, err = ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	// "naked return", possible because we have named return values
	return
}

// PageLoader is used to load pages
var PageLoader FileReader = &ioReader{}

// Load loads the page with the given title from the file system
// If the page cannot be loaded returns nil with the root cause
func Load(title string) (*Page, error) {
	body, err := PageLoader.ReadFile(makePagePath(title))
	if err != nil {
		return nil, err
	}
	return New(title, string(body)), nil
}
