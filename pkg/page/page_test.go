package page

import (
	"errors"
	"flag"
	"io/ioutil"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/nagypalg/gowiki/pkg/testsup"
)

type fakeReader struct {
	pathToContent map[string]string
}

var errUnknownPath = errors.New("Unknown")

// *fakeReader will also implement FileReader
func (r *fakeReader) ReadFile(path string) ([]byte, error) {
	content, ok := r.pathToContent[path]
	if ok {
		return []byte(content), nil
	}
	return nil, errUnknownPath
}

func TestLoad(t *testing.T) {
	assert := assert.New(t)

	pathToContent := map[string]string{
		filepath.Join("data", "foo.txt"): "foo",
		filepath.Join("data", "bar.txt"): "bar",
	}

	origLoader := PageLoader
	t.Log("Replacing PageLoader")
	PageLoader = &fakeReader{pathToContent}
	defer func() {
		t.Log("Restoring original PageLoader")
		PageLoader = origLoader
	}()

	loadtests := []struct {
		path    string
		expPage *Page
		expErr  error
	}{
		{"foo", New("foo", "foo"), nil},
		{"bar", New("bar", "bar"), nil},
		{"baz", nil, errUnknownPath},
	}

	for _, tt := range loadtests {
		// "Table-driven" subtests
		t.Run("Path "+tt.path+"", func(t *testing.T) {
			p, err := Load(tt.path)
			assert.Equal(tt.expPage, p)
			assert.Equal(tt.expErr, err)
		})

	}

}

func TestSave(t *testing.T) {
	assert := assert.New(t)
	// another example for returning a function from a function
	tmpdir, cleanup := testsup.CreateTempDir(t, "page_test")
	defer cleanup()

	origDataDir := DataDir
	DataDir = tmpdir
	defer func() { DataDir = origDataDir }()

	title := "TestTitle"
	body := "Test body"
	New(title, body).Save()

	savedFileName := filepath.Join(tmpdir, "TestTitle.txt")
	t.Logf("Temp file: %q", savedFileName)

	loaded, err := ioutil.ReadFile(savedFileName)
	t.Logf("Loaded: %q", loaded)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(body, string(loaded))

}

func TestNewNoBody(t *testing.T) {
	assert := assert.New(t)
	p := NewNoBody("title")
	// note that although the type of p is *Page, we can access its title Field w/o dereference
	assert.Equal("title", p.Title)
	assert.Nil(p.Body)
}

func TestMakePagePath(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(filepath.Join("data", "foo.txt"), makePagePath("foo"))
	assert.Equal(filepath.Join("data", "bar.txt"), makePagePath("bar"))
}

// built-in command-line flags parsing support!
var update = flag.Bool("update", false, "update .golden files")

func TestRenderBody(t *testing.T) {
	assert := assert.New(t)

	tests := []string{"simple", "pagelink", "markdown", "complex"}

	for _, tst := range tests {
		t.Run(tst, func(t *testing.T) {
			goldenPath := filepath.Join("testdata", "TestRenderBody", tst+".golden")
			body := testsup.LoadTestFile(t, "TestRenderBody", tst+".input")
			page := New("test", string(body))
			rendered := page.RenderBody()
			// this is a very interesting technique: with the -update flag we update the golden files
			// so we do not have to write them by hand
			if *update {
				t.Log("update golden file")
				if err := ioutil.WriteFile(goldenPath, []byte(rendered), 0644); err != nil {
					t.Fatalf("failed to update golden file %s: %s", goldenPath, err)
				}
			}
			expBody := testsup.LoadTestFile(t, "TestRenderBody", tst+".golden")
			assert.Equal(expBody, rendered)

		})
	}

}
