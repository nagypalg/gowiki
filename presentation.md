---
marp: true

---

<!-- 
$theme: gaia
template: invert
footer: Go GoLang!
-->

<!-- $size: 16:9 -->
<!-- page_number: true -->


Go GoLang!
===

# ![Go Logo 50%](https://golang.org/doc/gopher/frontpage.png)

##### Some highlights of Go for enterprise applications

###### Gabor Nagypal

<!-- 
*page_number: false 
*footer:
-->

---

# Very brief history of Go

- Designed in 2007 in Google by Robert Griesemer, Rob Pike, and Ken Thompson
- First public version in 2009, 1.0 in 2012
- Current version: 1.12, Go 2 in preparation
- Written in Go: Docker, Kubernetes, CockroachDB, Snappy, ...

---

# What is Go used for

- API/RPC/Web services (returning HTML or non-HTML)
- CLI programs
- Monitoring agents, automation scripts

NOT used for (yet?): Desktop/GUI, games, mobile apps

https://blog.golang.org/survey2018-results

---

# Why Go for enterprise apps?

**Pragmatic** language with the goal to develop **scalable** applications.
  - **Processing units**: built-in concurrency  and HTTP support in the standard library
  - **LOC**: interesting concepts for structuring code and dependency management, fast compilation (and startup), easy deployment
  - **Team size**: Major principles: **readability** and **simplicity**. 
  Enforces consistent formatting and documentation. 
  Strongly and statically typed.

---

# Some quotes

`
I like a lot of the design decisions they made in the [Go] language. 
Basically, I like all of them.
`

###### *Martin Odersky (creator of Scala)*


`Programming in Go is like being young again (but more productive!).`

`
go is not interesting at all. i debug 3 times less than before with it, where is all the fun? :(
`
###### *http://go-lang.cat-v.org/quotes*


---

# Code walkthrough #1


https://gitlab.com/nagypalg/gowiki/tree/master/cmd/hello

---

# What we have seen

- Strongly and statically typed. (like Java :ok:)

- Compiled. (unlike Java, bad :-1:)
  - but it is easy to compile to other platforms :ok:

- Produces a **single binary** without further dependencies. :+1:
  - easy deployment (unlike Java, good :+1:)

- Enforces clean code, unused variables and imports are **compile errors**. :+1:

---

# What we have seen (2)

- Organizes code into packages :ok:

- Can use remote packages (dependency management, modules) :+1:
  - Frequently real URLs (global godoc.org) :+1:

- Compact with type and semicolon inference. :+1:

- Easy concurrency with goroutines and channels :+1:

- Managed memory (GC) :ok:

---

# Code walkthrough

https://gitlab.com/nagypalg/gowiki/tree/master/cmd/goserve

https://gitlab.com/nagypalg/gowiki/tree/master/pkg/testsup

---

# What we have seen

- Has collections (slice, map) :ok:
  - but only these, e.g. no sets :-1:

- Has variadic parameters :ok:

- "Batteries included" standard library :+1:
  - HTTP, file utilities, testing, logging

- Multiple return values :+1:

- Simple visibility rules :+1:

---

# What we have seen (2)

- Easy testing
  - subtests :+1:
  - assertion library available :ok:

- Defer for cleanup :+1:

- Only explicit type casts :+1: :-1:

- Enforces formatting and documentation (by standard go tools) :+1:

- Built-in profiling support :+1:

---

# Code walkthrough

https://gitlab.com/nagypalg/gowiki/tree/master/pkg/page

https://gitlab.com/nagypalg/gowiki/tree/master/cmd/gowiki

---

# What we have seen

- Has interfaces and methods. :ok: 
  
- But does not have classes and inheritance. :+1: :-1:

- Interfaces implemented implicitly. :+1: :-1:

- No polymorpism, function/method names have to be unique. :-1:


---

# What we have seen (2)

- Functions are first class citizens. :+1:
  
- But not an FP language, does not support immutability. :-1:

- Templating :+1:

- CLI flags, golden files testing technique :+1:

---

# What we have NOT seen

- reflection

- "annotations" (aka tags): used for JSON, ORM, ...

- switch, type switch

- panic, recover

- ...

---

# What's next?

- [A more elaborate tutorial by me](https://gitlab.com/nagypalg/gotour4javadevs)
- [A Tour of Go](https://tour.golang.org)
- [Learn X in Y minutes for Go](https://learnxinyminutes.com/docs/go/)
- [Go by Example](https://gobyexample.com/)

---

## Thanks for your time! :smiley: