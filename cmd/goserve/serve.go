/*
the file can be named arbitrarily, but the executable will have the name of the
enclosing directory, i.e. "goserve"
*/
package main

// Go has a standard formatter called gofmt
// so you do not have to care about code formatting conventions
import (
	"log"              // built-in logging
	"net/http"         // built-in HTTP server
	_ "net/http/pprof" // for profiling
	"os"               // to access OS functions
	"strconv"          // to convert from and to strings
	"strings"          // string manipulation utilities
)

// all declarations can be grouped
const (
	defaultDir  = "."
	defaultPort = ":7654"
)

func dir() (string, error) {
	// although the main method does not have args parameters
	// there is a way to access them
	// os.Args[0] is the command name
	if len(os.Args) > 1 {
		d := os.Args[1] // and this is the first command-line parameter
		_, err := os.Stat(d)
		return d, err
	}
	return defaultDir, nil
}

func port() string {
	// if we want, we can declare variables separately
	// but even in this case they get initialized with their zero value (here nil)
	var err error
	// port is visible only in the if block
	if port := os.Getenv("PORT"); len(port) > 0 {
		// Go has multiple returns!
		// If we want to ignore some of them, we can use the _ notation
		_, err = strconv.Atoi(port)
		// there are no exceptions, this is the typical error handling used instead
		if err != nil {
			log.Printf("Could not parse provided port %q, using default", port)
			return defaultPort
		}
		return ":" + port
	}
	return defaultPort
}

// Go has pointers (note the *), but no pointer arithmetic, so it is safe
// Pointers are needed because Go always passes parameters by value
func setupHandlers(dir string) *http.ServeMux {
	mux := http.NewServeMux()
	// We will serve the content of the directory
	fs := http.FileServer(http.Dir(dir))
	// / means "all paths", if multiple path apply, the most specific wins
	mux.Handle("/", fs)
	return mux
}

func main() {

	// to start profiler interface
	// http://localhost:6060/debug/pprof/
	go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()

	dir, err := dir()
	if err != nil {
		// fatal level logging will exit the program
		log.Fatalf("Cannot serve %q: %s", dir, err)
	}
	port := port()
	log.Printf("%s will serve the directory %q on port %s", os.Args[0], dir, strings.TrimPrefix(port, ":"))
	// built-in HTTP server!
	// if there is some error while starting it up we log it and exit
	log.Fatal(http.ListenAndServe(port, setupHandlers(dir)))
	// note: serving HTTPS is not much more complicated, check https://pocketgophers.com/serving-https/

}
