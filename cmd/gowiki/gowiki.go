package main

import (
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"text/template"

	"gitlab.com/nagypalg/gowiki/pkg/page"
)

const defaultPort = 8000

var templateNames = []string{"edit", "view"}

// built-in templating!
var templates = template.Must(template.ParseFiles(enrichTemplateNames(templateNames)...))

var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9]+)$")

// Go has no (built-in) filter, map, reduce, ... operations
// but it is fairly easy to write the same functionality
// See also https://medium.com/@zach_johnson/a-simple-implementation-of-filtering-logic-in-go-f45a24413aaf
// which describes how to write your own filter "framework" in few lines of code, if needed
func enrichTemplateNames(names []string) []string {
	res := make([]string, len(names))
	for i, name := range names {
		res[i] = "tmpl/" + name + ".html"
	}
	return res
}

// type HandlerFunc func(ResponseWriter, *Request) -> it is a type alias in the http package
func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	// functions are first class citizens, here we return a function!
	// also note that http.HandlerFunc is a type alias for a function signature
	return func(w http.ResponseWriter, r *http.Request) {
		// regexp support is there, too
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			errMsg := "Invalid URL '" + r.URL.Path[1:] + "'"
			log.Println(errMsg)
			return
		}
		fn(w, r, m[2])
	}
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/view/FrontPage", http.StatusFound)
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	// note: not PageLoad
	p, err := page.Load(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := page.Load(title)
	if err != nil {
		p = page.NewNoBody(title)
	}
	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := page.New(title, body)
	// method invocation
	err := p.Save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *page.Page) {
	// the template receives a page struct and can refer to its fields and methods
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func port() (portnum int) {
	cmd := os.Args[0]
	var err error
	portnum = defaultPort
	if len(os.Args) > 1 {
		provided := os.Args[1]
		portnum, err = strconv.Atoi(provided)
		if err != nil {
			log.Printf("Could not parse provided port %s, using default", provided)
			portnum = defaultPort
		}
	}
	log.Printf("%s is going to listen on port %d", cmd, portnum)
	return
}

func setupHandlers() *http.ServeMux {
	mux := http.NewServeMux()

	mux.HandleFunc("/", rootHandler)
	mux.HandleFunc("/view/", makeHandler(viewHandler))
	mux.HandleFunc("/edit/", makeHandler(editHandler))
	mux.HandleFunc("/save/", makeHandler(saveHandler))

	return mux
}

func main() {
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(port()), setupHandlers()))
}
