// Multiple files can contribute to the same package
// but everything will be compiled into ONE binary file
package main

import (
	"fmt"

	// we can easily use external packages
	// note that the URL can be opened in the browser (in most cases)
	"rsc.io/quote"
)

// init is executed after global constants and variables are initalized
// but before the main method
func init() {

	fmt.Println("Init of quotes.go")

	// we can use "string interpolation" even in raw strings
	fmt.Printf(msg, "Go")

	// most types have a sensible string representation
	fmt.Println("Started at", started)

	// unused := "will not compile"
}

func sendQuotes(c chan string) {
	// <- sends to the channel
	c <- quote.Go()
	c <- quote.Hello()
	c <- quote.Glass()
	c <- quote.Opt()
	close(c)
}
