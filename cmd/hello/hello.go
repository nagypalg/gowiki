// executables must be in the main package
package main

// we can use other packages
import (
	"fmt"
	"time"
)

// Go has raw strings!
// Note that type declaration is not needed
// constants can only have primitive types (string, numeric, boolean)
const msg = `Hello
multiline
%s
`

// constant expressions perform arithmetic with arbitrary precision
// also see https://gobyexample.com/constants
// constants can only have primitive types (string, numeric, boolean)
const big = 3e2000000 // this will not fit even into float64
const n = 3e1999999

// Global variable definition
// Note that type is not declared
// Below line would be valid Go but the linter would complain
// var started time.Time = time.Now()
var started = time.Now()

// executables must have a main method
func main() {

	fmt.Println("main of hello.go")

	// fmt.Println("big=", big) will not work because big will not fit into any numeric type
	// but the below works and prints out 10, as expected
	fmt.Println("big/n=", big/n)

	// this creates a channel, it is like a Java BlockingQueue
	// note that types of variables are inferred automatically in the most cases
	// := is a common shorthand for variable declaration and value assigment,
	// can be used only in function bodies
	qc := make(chan string)

	// this starts a new goroutine
	go sendQuotes(qc)

	fmt.Println("Received quotes:")

	// this receives from the channel until it is closed
	for q := range qc {
		// C-style printf
		fmt.Printf("%q\n", q)
	}
}
