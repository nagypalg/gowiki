1. `cd ../gowiki`
1. `ls -la` - no binary
1. `go install`
1. `ls -la` - still no binary
1. `which gowiki` - it is in $GOPATH/bin (which is usually in $PATH)
1. `gowiki` - note that creates the data directory
1. `gowiki 9000` - note that it does not create the directory and start at port 9000
1. Open browser with `http://localhost:9000` and demo the wiki app
    * Page links
    * Automatic editing of new pages
    * Markdown support
1. Show the files in `data`
1. Show `gowiki.go`
1. Show `page.go` when reaching the view handler, then go back to `gowiki.go`
1. Show `view.html` when reaching renderTemplate, then go back to `gowiki.go`
1. Show `page_test.go``
1. Show `testdata/TestRenderBody` when reaching TestRenderBody
1. `cd ../..`
1. `go test ./...`
1. `go test ./... -v -update` - show that golden files were not changed on git
1. `go test ./... -v`
