1. `cd ../goserve`
1. `ls -la` - there are two go files, no binaries
1. `go run serve.go` - note that strange long command path
1. `ls -la` - there are still no binaries
1. `go build && ./goserve testdata`
1. `PORT=7777 ./goserve testdata`
1. Show the files in `testdata`
1. Open browser with `http://localhost:7777`
1. Open `serve.go`
1. Run tests: `go test -v`
1. Open `serve_test.go`
1. Show `go.mod`
1. Open `testsup.go`when reaching the first HTTPGet test method and explain NewGetRequest