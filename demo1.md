1. `cd cmd/hello`
1. `ls -la` - there are two go files
1. `go build` - one binary
1. `./hello.exe` or `./hello`
1. `./buildlinux` - copy hello.bin to a Linux machine and execute it
1. `./buildwasm` - result will be used in the next demo, explain `-o`option
1. Open `hello.go` and explain the comments
1. Open `quotes.go` and explain the comments
1. `go clean`
1. Show that the binary for the current platform was deleted